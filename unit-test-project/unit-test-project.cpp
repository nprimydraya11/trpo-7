#include "pch.h"
#include "CppUnitTest.h"
#include "../main-project/exchange_rates.h"
#include "../main-project/processing.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestproject
{


	exchange_rates* build_rate(double buy, double sell)
	{
		exchange_rates* rate = new exchange_rates;
		rate->buy = buy;
		rate->sell = sell;
		return rate;
	}

	//       
	void delete_rates(exchange_rates* array[], int size)
	{
		for (int i = 0; i < size; i++)
		{
			delete array[i];
		}
	}





	TEST_CLASS(unittestproject)
	{
	public:
		
		TEST_METHOD(TestMethod1) //     
		{
			exchange_rates* subscriptions[3];
			subscriptions[0] = build_rate(43.75, 123.45); // 79.7
			subscriptions[1] = build_rate(0.35, 0); //
			subscriptions[2] = build_rate(567.345, 678); // 
			Assert::AreEqual(110.655, process(subscriptions, 3));
			delete_rates(subscriptions, 3);
		}

		TEST_METHOD(TestMethod2) //     
		{
			exchange_rates* subscriptions[3];
			subscriptions[0] = build_rate(34.5, 20.5); // 
			subscriptions[1] = build_rate(4569.0, 234.0); // 
			subscriptions[2] = build_rate(845.0, 2.0); // 
			Assert::AreEqual(843.0, process(subscriptions, 3));
			delete_rates(subscriptions, 3);
		}

		TEST_METHOD(TestMethod3) //     
		{
			exchange_rates* subscriptions[3];
			subscriptions[0] = build_rate(20000.0, 456223451.0); //  
			subscriptions[1] = build_rate(1245163451.0, 416341234.0); //  
			subscriptions[2] = build_rate(135413463.0, 64345.0); //  
			Assert::AreEqual(135349118.0, process(subscriptions, 3));
			delete_rates(subscriptions, 3);
		}

	};
}
