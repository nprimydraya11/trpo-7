#include <iostream>
#include <iomanip>

using namespace std;

#include "exchange_rates.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include "processing.h"

void output(exchange_rates* rate)
{

	cout << "���� ";

	cout << rate->name << " ";

	cout << '"' << rate->adress << '"';
	cout << '\n';


	cout << "������� ";
	cout << setw(1) << setfill('0') << rate->buy << ' ';
	cout << '\n';


	cout << "������� ";
	cout << setw(2) << setfill('0') << rate->sell << '\t';
	cout << '\n';
	cout << '\n';
}

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "������������ ������ �8. GIT\n";
	cout << "������� �4. ����� �����\n";
	cout << "�����: ��������� �������\n";
	cout << "������: 16\n\n";
	exchange_rates* rates[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", rates, size);
		cout << "***** ����� ����� *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(rates[i]);
		}
		bool (*check_function)(exchange_rates*) = NULL; // check_function - ��� ��������� �� �������, ������������ �������� ���� bool,
		                                                   // � ����������� � �������� ��������� �������� ���� exchange_rates*
		cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
		cout << "1) ��� ��������� ������������\n";
		cout << "2) ����� ������ � ������ ��������� ������, � ������� ������� ������ 2,5\n";
		cout << "3) ������������ ������� ����� �������� � �������� ������ � ����� ������� ";
			cout << "\n������� ����� ���������� ������: ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_by_name; //       
			cout << "---\n\n";
			break;
		case 2:
			check_function = check_by_key; //       
			cout << " \n\n";
			break;
		case 3:
		{
			cout << process(rates, size) << "\n\n";
			break;
		}
		default:
			throw "  ";
		}
		if (check_function)
		{
			int new_size;
			exchange_rates** filtered = filter(rates, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete rates[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
