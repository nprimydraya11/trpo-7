#ifndef EXCHANGE_RATES_H
#define EXCHANGE_RATES_H

#include "constants.h"


struct exchange_rates
{
	char name[MAX_STRING_SIZE];
	double buy;
	double sell;
	char adress[MAX_STRING_SIZE];
};

#endif
