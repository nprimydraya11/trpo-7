#include "processing.h"
#include "iostream"
using namespace std;


double diff(double x, double y)
{
	return (x > y ? x - y : y - x);
}

double process(exchange_rates* array[], int size)
{
	double max = diff(array[0]->buy, array[0]->sell);
	for (int i = 1; i < size; i++)
	{
		double curr = diff(array[i]->buy, array[i]->sell);
		if (curr > max)
		{
			max = curr;
		}
	}
	return max;
}