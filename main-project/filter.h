#ifndef FILTER_H
#define FILTER_H

#include "exchange_rates.h"

exchange_rates** filter(exchange_rates* array[], int size, bool (*check)(exchange_rates* element), int& result_size);

bool check_by_name(exchange_rates* element);

bool check_by_key(exchange_rates* element);

#endif

