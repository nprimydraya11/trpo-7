#ifndef PROCESSING_H
#define PROCESSING_H

#include "exchange_rates.h"

double process(exchange_rates* array[], int size);

#endif
